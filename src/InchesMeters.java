import java.util.Scanner;

public class InchesMeters {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a value for inch: ");
        double inches = in.nextInt();
        double meters;

        final double INCHES_PER_CENTIMETER = 2.54;
        final double INCHES_PER_METER = INCHES_PER_CENTIMETER / 100;

        meters = inches * INCHES_PER_METER;
        System.out.println(inches  + " inches - is " + meters + " meters");
    }
}
